#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <ACROBOTIC_SSD1306.h>
#include "wifi.h"

#define LICZBA_RFID 4
#define MAX_OPOZNIENIE 500

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASS;

SoftwareSerial** RFID;
byte* opoznienie;
bool czy_naglowek_o_wykryciu;
String naglowek;

WiFiServer server(80);
WiFiServer server2(1900);

unsigned data1;

void setup() {
	Serial.begin(9600);
	Wire.begin();
	oled.init();
	oled.clearDisplay();
	oled.setTextXY(0, 0);

	RFID = new SoftwareSerial*[LICZBA_RFID];

	RFID[0] = new SoftwareSerial(D5, D3);
	RFID[0]->begin(9600);
	RFID[1] = new SoftwareSerial(D6, D3);
	RFID[1]->begin(9600);
	RFID[2] = new SoftwareSerial(D7, D3);
	RFID[2]->begin(9600);
	RFID[3] = new SoftwareSerial(D4, D3);
	RFID[3]->begin(9600);

	Serial.println("RFID Ready to listen");

	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("");

	Serial.println("WiFi connected");
	server.begin();
	server2.begin();
	Serial.print("Server started: ");
	Serial.println(WiFi.localIP());

	oled.setTextXY(7, 0);
	oled.putString(WiFi.localIP().toString());
	delay(5000);

	opoznienie = new byte[LICZBA_RFID];
	for (byte i = 0; i < LICZBA_RFID; i++)
		opoznienie[i] = 0;
	czy_naglowek_o_wykryciu = false;

	oled.clearDisplay();
	oled.setTextXY(0, 0);
	oled.putString("Brak tagow");
}

String readTag(SoftwareSerial *rfid)
{
	String msg = "";

	if (rfid->available() > 0)
	{
		delay(100); // needed to allow time for the data to come in from the serial buffer.
		data1 = rfid->read();
		if (data1 == 2) { // transmission begins
			for (int z = 0; z < 12; z++) // read the rest of the tag
			{
				data1 = rfid->read();
				//delay(100);
				msg += char(data1);
			}
		}
		data1 = rfid->read();
		rfid->flush();
		if (data1 == -1) { //end of transmission -- bylo 3, ale zmienilem na -1, bo na koncu jest zawsze -1
			return msg;
		}
	}
	return "";
}

WiFiClient client2;
byte last_maxo = -1;

void loop() {
	String message = "";

	for (byte i = 0; i < LICZBA_RFID; i++)
	{
		String tagID = readTag(RFID[i]);
		if (tagID != "") {
			opoznienie[i] = MAX_OPOZNIENIE;

			Serial.print("Wykryto czytnik nr ");
			Serial.println(i + 1);
		}
		else
			if (opoznienie[i] > 0)
				opoznienie[i]--;
	}

	byte puste_czytniki = 0;
	for (byte i = 0; i < LICZBA_RFID; i++)
	{
		if (opoznienie[i] != 0)
		{
			if (czy_naglowek_o_wykryciu == false)
			{
				czy_naglowek_o_wykryciu = true;
				oled.clearDisplay();
				oled.setTextXY(0, 0);
				naglowek = "Wykryto tag przy czytniku: ";
				oled.putString("Wykryto tag przy czytniku: ");
				break;
			}
		}
		else
			puste_czytniki++;
	}

	String numery = "";
	if (puste_czytniki == 4)
	{
		if (czy_naglowek_o_wykryciu == true)
		{
			czy_naglowek_o_wykryciu = false;
			oled.clearDisplay();
			oled.setTextXY(0, 0);
			naglowek = "Brak tagow";
			oled.putString("Brak tagow");
		}
	}
	else
	{
		for (byte i = 0; i < LICZBA_RFID; i++)
		{
			if (opoznienie[i] > 0)
			{
				numery += i;
				numery += " ";
			}
			// numery+="                   ";
		}
		oled.setTextXY(2, 0);
		oled.putString(numery);
	}

	WiFiClient client = server.available();
	if (client.connected())
	{
		client.println("HTTP/1.1 200 OK");
		client.println("Content-Type: text/html");
		client.println("Connection: close");
		client.println("Refresh: 1");
		client.println();
		client.println("<!DOCTYPE HTML>");
		client.println("<html>");
		client.println("<head></head><body><h1>Witaj uzytkowniku!</h1><h3>");
		String s = "";// s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html><header><title>RFID </title></header><body>\r\n";
		s += naglowek;
		s += numery;
		client.print(s);
		client.flush();

		delay(100);
	}
	client.stop();

	if (!client2)
		client2 = server2.available();

	if (client2)
	{
		if (client2.connected())
		{
			if (numery.length() != 0) {
				byte maxo = 0;
				for (uint8_t i = 1; i < LICZBA_RFID; i++)
				{
					if (opoznienie[i] > opoznienie[maxo])
						maxo = i;
				}

				if (last_maxo != maxo)
					client2.println(maxo);

				last_maxo = maxo;
			}
		}
		else
		{
			client2.stop();
		}
	}

	yield();
}